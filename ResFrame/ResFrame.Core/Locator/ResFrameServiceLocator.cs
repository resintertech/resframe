﻿using System;
using System.Threading;
using ResFrame.Core.ComponentRegistry;
using ResFrame.Core.Job;
using StructureMap;

namespace ResFrame.Core.Locator
{
    public class ResFrameServiceLocator
    {
        private static readonly Lazy<Container> ContainerBuilder = new Lazy<Container>(DefaultContainer,
            LazyThreadSafetyMode.ExecutionAndPublication);

        public static IContainer Container
        {
            get { return ContainerBuilder.Value; }
        }

        private static Container DefaultContainer()
        {
            return new Container(a =>
            {
                //registry all in here
                a.AddRegistry(new GeneralRegistry());
                a.AddRegistry(new WebRegistry());
                a.AddRegistry(new ControllerRegistry());
                a.AddRegistry(new ActionFilterRegistry(
                    () => Container));
                a.AddRegistry(new JobRegistry());
            });
        }
    }
}