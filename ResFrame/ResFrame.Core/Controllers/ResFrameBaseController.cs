﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Microsoft.Web.Mvc;
using ResFrame.Core.ActionResult;

namespace ResFrame.Core.Controllers
{
    public abstract class ResFrameBaseController : Controller
    {
        protected System.Web.Mvc.ActionResult RedirectToAction<TController>(Expression<Action<TController>> action)
           where TController : ResFrameBaseController
        {
            return ControllerExtensions.RedirectToAction(this, action);
        }

        [Obsolete("Do not use the standard Json helpers to return JSON data to the client.  Use either JsonSuccess or JsonError instead.")]
        protected JsonResult Json<T>(T data)
        {
            throw new InvalidOperationException("Do not use the standard Json helpers to return JSON data to the client.  Use either JsonSuccess or JsonError instead.");
        }

        protected CommonJsonResult JsonValidationError()
        {
            var result = new CommonJsonResult();

            foreach (var validationError in ModelState.Values.SelectMany(v => v.Errors))
            {
                result.AddError(validationError.ErrorMessage);
            }
            return result;
        }

        protected CommonJsonResult JsonError(string errorMessage)
        {
            var result = new CommonJsonResult();

            result.AddError(errorMessage);

            return result;
        }

        protected CommonJsonResult<T> JsonSuccess<T>(T data)
        {
            return new CommonJsonResult<T> { Data = data };
        }
    }

    public class CommonJsonResult<T> : CommonJsonResult
    {
        public new T Data
        {
            get { return (T)base.Data; }
            set { base.Data = value; }
        }
    }
}