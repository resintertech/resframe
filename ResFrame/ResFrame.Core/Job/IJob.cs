﻿namespace ResFrame.Core.Job
{
    public interface IJob
    {
        void Execute();
    }
}