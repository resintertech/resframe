﻿using StructureMap;
using StructureMap.Graph;

namespace ResFrame.Core.Job
{
    public class JobRegistry : Registry
    {
        public JobRegistry()
        {
            Scan(a =>
            {
                a.AssembliesFromApplicationBaseDirectory(b => b.FullName.StartsWith("ResFrame"));
                a.AddAllTypesOf<IJob>();                
            });
        }
    }
}