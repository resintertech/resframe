﻿using StructureMap;
using StructureMap.Graph;

namespace ResFrame.Core.ComponentRegistry
{
    public class ControllerRegistry : Registry
    {
        public ControllerRegistry()
        {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.With(new ControllerConvention());
            });
        }
    }
}