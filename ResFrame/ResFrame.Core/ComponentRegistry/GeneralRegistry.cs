﻿using StructureMap;
using StructureMap.Graph;

namespace ResFrame.Core.ComponentRegistry
{
    public class GeneralRegistry : Registry
    {
        public GeneralRegistry()
        {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.Assembly("ResFrame.Web");
                scan.Assembly("ResFrame.Access");
                scan.WithDefaultConventions();
            });
        }
    }
}