﻿using System;
using System.Web.Mvc;
using ResFrame.Core.FilterProvider;
using StructureMap;
using StructureMap.TypeRules;

namespace ResFrame.Core.ComponentRegistry
{
    public class ActionFilterRegistry : Registry
    {
        public ActionFilterRegistry(Func<IContainer> container)
        {
            For<IFilterProvider>().Use(new ResFrameFilterProvider(container));

            Policies.SetAllProperties(a =>
                a.Matching(b =>
                    b.DeclaringType.CanBeCastTo(typeof(ActionFilterAttribute)) &&
                    b.DeclaringType.Namespace.StartsWith("ResFrame") &&
                    !b.PropertyType.IsPrimitive &&
                    b.PropertyType != typeof(string)));
        }
    }
}