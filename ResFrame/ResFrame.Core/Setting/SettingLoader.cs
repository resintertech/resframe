﻿using System.IO;
using Newtonsoft.Json;

namespace ResFrame.Core.Setting
{
    public class SettingLoader
    {
        public static Setting GetSiteSetting()
        {
            using (var file = File.OpenText("../SiteSetting/site.setting"))
            {
                var serializer = new JsonSerializer();
                var setting = (Setting)serializer.Deserialize(file, typeof(Setting));
                return setting;
            }
        }
    }
}