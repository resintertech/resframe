﻿namespace ResFrame.Core.Setting
{
    public class Setting
    {
        public string EncryptionKey { get; set; }
        public string EncryptionIv { get; set; }
        public string SaltString { get; set; }
    }
}