﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ResFrame.Core.Security
{
    public abstract class BaseSecurity
    {
        public virtual string Hashed(string clear, string salt)
        {
            var hashAlgo = new MD5CryptoServiceProvider();
            var completeBase = Encoding.ASCII.GetBytes(clear + salt);
            var encryptedPass = hashAlgo.ComputeHash(completeBase);
            var encryptedString = Convert.ToBase64String(encryptedPass);
            return encryptedString;
        }

        public virtual string CreateSalt()
        {
            var randByte = new byte[16];
            var random = RandomNumberGenerator.Create();
            random.GetBytes(randByte);
            return Convert.ToBase64String(randByte);
        }

        public virtual string Encrypt(string clear, byte[] key, byte[] iv)
        {
            byte[] encrypted;
            using (var rijndael = Rijndael.Create())
            {
                rijndael.IV = iv;
                rijndael.Key = key;
                var encryptor = rijndael.CreateEncryptor(rijndael.Key, rijndael.IV);

                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(clear);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(encrypted);
        }

        public virtual string Decrypt(string encrypted, byte[] key, byte[] iv)
        {
            var decrypted = "";
            var encByte = Convert.FromBase64String(encrypted);
            using (var rijndael = Rijndael.Create())
            {
                rijndael.Key = key;
                rijndael.IV = iv;
                var decryptor = rijndael.CreateDecryptor(rijndael.Key, rijndael.IV);

                using (var msDecrypt = new MemoryStream())
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srEncrypt = new StreamReader(csDecrypt))
                        {
                            decrypted = srEncrypt.ReadToEnd();
                        }
                    }
                }
            }
            return decrypted;
        }
    }
}