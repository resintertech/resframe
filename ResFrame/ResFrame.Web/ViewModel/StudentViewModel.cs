﻿namespace ResFrame.Web.ViewModel
{
    public class StudentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string StudentClass { get; set; }
        public string Address { get; set; } 
    }
}