﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ResFrame.Access.Repositories.Abstraction;
using ResFrame.Core.Controllers;
using ResFrame.Web.ViewModel;

namespace ResFrame.Web.Controllers
{
    public class HomeController : ResFrameBaseController
    {
        private readonly IStudentRepository _studentRepository;

        public HomeController(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        [Authorize]
        public ActionResult Index()
        {
            var model = _studentRepository.GetAll().Select(a => new StudentViewModel()
            {
                Name = a.Name,
                Address = a.Address,
                StudentClass = a.StudentClass,
                Id = a.Id
            });
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}