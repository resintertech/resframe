﻿using System.Web.Mvc;
using System.Web.Security;
using ResFrame.Access.Repositories.Abstraction;
using ResFrame.Core.Controllers;
using ResFrame.Domain.DbModel;
using ResFrame.Web.Security;
using ResFrame.Web.ViewModel;

namespace ResFrame.Web.Controllers
{
    public class AccountController : ResFrameBaseController
    {
        private readonly IUserRepository _userRepository;
        private AppSecurity _security;

        public AccountController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
            _security = new AppSecurity();
        }

        public void CreateUser()
        {
            var salt = _security.CreateSalt();
            var user = new User()
            {
                Username = "reynaldi",
                Password = _security.Hashed("password", salt),
                Salt = salt
            };
            _userRepository.Insert(user);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginView model)
        {
            var user = _userRepository.GetUserByName(model.Username);
            if (user == null)
            {
                return View(model);
            }
            if (user.Password == _security.Hashed(model.Password, user.Salt))
            {
                FormsAuthentication.SetAuthCookie(model.Username, true);
                return RedirectToAction<HomeController>(a => a.Index());
            }   
            return View(model);         
        }
    }
}