﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ResFrame.Core.Job;
using ResFrame.Core.Locator;
using ResFrame.Core.Resolver;
using StructureMap;

namespace ResFrame.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static IContainer Container
        {
            get { return (IContainer)HttpContext.Current.Items["_Container"]; }
            set { HttpContext.Current.Items["_Container"] = value; }
        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);            
            DependencyResolver.SetResolver(new ResFrameDependencyResolver(ResFrameServiceLocator.Container));
            using (var container = ResFrameServiceLocator.Container.GetNestedContainer())
            {
                foreach (var task in container.GetAllInstances<IRunAtInit>())
                {
                    task.Execute();
                }
                foreach (var task in container.GetAllInstances<IRunAtStartup>())
                {
                    task.Execute();
                }
            }
        }

        protected void Application_BeginRequest()
        {
            Container = ResFrameServiceLocator.Container.GetNestedContainer();

            foreach (var task in Container.GetAllInstances<IRunAfterRequest>())
            {
                task.Execute();
            }
        }

        protected void Application_Error()
        {
            foreach (var task in Container.GetAllInstances<IRunAtError>())
            {
                task.Execute();
            }
        }

        protected void Application_EndRequest()
        {
            try
            {
                var container = Container.GetAllInstances<IRunAfterRequest>();
                foreach (var task in container)
                {
                    task.Execute();
                }
            }
            finally
            {
                Container.Dispose();
                Container = null;
            }
        }
    }
}
