﻿using ResFrame.Domain.DbModel;

namespace ResFrame.Access.Repositories.Abstraction
{
    public interface IStudentRepository : IGeneralRepository<Student>
    {
         
    }
}