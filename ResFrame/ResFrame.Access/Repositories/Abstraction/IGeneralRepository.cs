﻿using System.Collections.Generic;
using ResFrame.Domain;

namespace ResFrame.Access.Repositories.Abstraction
{
    public interface IGeneralRepository<T> where T : BaseModel
    {
        T Insert(T newData);
        T Update(T updatedData);
        bool Delete(int dataId);
        T Search(int dataId);
        IEnumerable<T> GetAll();
    }
}