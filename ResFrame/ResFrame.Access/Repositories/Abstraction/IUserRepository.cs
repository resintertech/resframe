﻿using ResFrame.Domain.DbModel;

namespace ResFrame.Access.Repositories.Abstraction
{
    public interface IUserRepository : IGeneralRepository<User>
    {
        User GetUserByName(string username);
    }
}