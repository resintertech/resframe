﻿using ResFrame.Access.Context;
using ResFrame.Access.Repositories.Abstraction;
using ResFrame.Domain.DbModel;

namespace ResFrame.Access.Repositories.Implementation
{
    public class StudentRepository : GeneralRepository<Student, IResFrameContext>, IStudentRepository
    {
        public StudentRepository(IResFrameContext db) : base(db)
        {
        }
    }
}