﻿using System;
using System.Linq;
using ResFrame.Access.Context;
using ResFrame.Access.Repositories.Abstraction;
using ResFrame.Domain.DbModel;

namespace ResFrame.Access.Repositories.Implementation
{
    public class UserRepository : GeneralRepository<User, IResFrameContext>, IUserRepository
    {
        public UserRepository(IResFrameContext db) : base(db)
        {
        }

        public User GetUserByName(string username)
        {
            return Db.Users.FirstOrDefault(a => a.Username.ToLower() == username.ToLower());
        }        
    }
}