﻿using System.Collections.Generic;
using System.Data.Entity;
using ResFrame.Access.Context;
using ResFrame.Access.Repositories.Abstraction;
using ResFrame.Domain;

namespace ResFrame.Access.Repositories.Implementation
{
    public abstract class GeneralRepository<T, TContext> : IGeneralRepository<T>
        where T : BaseModel
        where TContext : IResFrameContext
    {
        protected readonly TContext Db;

        protected GeneralRepository(TContext db)
        {
            Db = db;
        }

        public virtual T Insert(T newData)
        {
            Db.Set<T>().Add(newData);
            return Db.SaveStatus() ? newData : null;
        }

        public virtual T Update(T updatedData)
        {
            Db.Entry(updatedData).State = EntityState.Modified;
            return Db.SaveStatus() ? updatedData : null;
        }

        public virtual bool Delete(int dataId)
        {
            var search = Search(dataId);
            Db.Set<T>().Remove(search);
            return Db.SaveStatus();
        }

        public virtual T Search(int dataId)
        {
            return Db.Set<T>().Find(dataId);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Db.Set<T>();
        }
    }
}