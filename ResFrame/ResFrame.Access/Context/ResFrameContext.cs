﻿using System;
using System.Data.Entity;
using ResFrame.Domain.DbModel;

namespace ResFrame.Access.Context
{
    public class ResFrameContext : DbContext, IResFrameContext
    {
        public ResFrameContext() : base("Data Source=.\\SQLEXPRESS;Initial Catalog=SampleDb;Integrated Security=True;MultipleActiveResultSets=True")
        {
            
        }
        public IDbSet<User> Users { get; set; }
        public IDbSet<Student> Students { get; set; }

        public bool SaveStatus()
        {
            try
            {
                SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}