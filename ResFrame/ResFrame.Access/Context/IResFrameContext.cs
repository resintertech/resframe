﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using ResFrame.Domain.DbModel;

namespace ResFrame.Access.Context
{
    public interface IResFrameContext : IDisposable, IObjectContextAdapter
    {
        IDbSet<User> Users { get; set; }
        IDbSet<Student> Students { get; set; }
            
        Database Database { get; }

        int SaveChanges();
        bool SaveStatus();
        DbSet<T> Set<T>() where T : class;
        DbEntityEntry Entry(object entity);
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}